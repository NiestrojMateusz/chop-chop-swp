export const routes = {
  MAIN: '/',
  LOGIN: '/login',
  POST: '/post/:id',
};

export const apiRoutes = {
  AUTH: '/auth',
  GET_POSTS: '/posts',
  ADD_COMMENT: '/comments',
  GET_AUTHOR: '/author',
  UPDATE_TIME: '/time',
};
