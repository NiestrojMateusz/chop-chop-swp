export function checkIsAuth() {
  const token = localStorage.getItem('token');

  return !!token;
}
