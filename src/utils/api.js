import axios from 'axios';

const api = axios.create({
  baseURL: 'https://edu-api.chop-chop.org/',
  responseType: 'json',
});

api.interceptors.request.use(
  function(config) {
    const token = localStorage.getItem('token');
    config.headers.common['X-Token'] = token || null;
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default api;
