import React, { useState } from 'react';
import './App.scss';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { routes } from './routes';
import MainTemplate from './templates/MainTemplate';
import Login from './pages/Login/Login';
import MainPage from './pages/MainPage/MainPage';
import Post from './pages/Post/Post';
import PostsContext from './context/posts.context';
import AuthContext from './context/auth.context';
import { checkIsAuth } from './utils/helpers';

function App() {
  const [posts, setPosts] = useState([]);
  const [isAuth, setIsAuth] = useState(checkIsAuth());

  return (
    <AuthContext.Provider value={{ isAuth, setIsAuth }}>
      <PostsContext.Provider value={{ posts, setPosts }}>
        <Router>
          <MainTemplate>
            <Switch>
              <Route
                exact
                path={routes.MAIN}
                render={() => (
                  <div>
                    {isAuth ? <MainPage /> : <Redirect to={routes.LOGIN} />}
                  </div>
                )}
              />
              <Route path={routes.LOGIN} component={Login} />
              <Route path={routes.POST} render={props => <Post {...props} />} />
            </Switch>
          </MainTemplate>
        </Router>
      </PostsContext.Provider>
    </AuthContext.Provider>
  );
}

export default App;
