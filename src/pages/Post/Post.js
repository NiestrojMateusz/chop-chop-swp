import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import styles from './Post.module.scss';
import PostsContext from '../../context/posts.context';
import api from '../../utils/api';
import { apiRoutes } from '../../routes';
import CommentModal from '../../components/CommentModal/CommentModal';
import AuthorInfoModal from '../../components/AuthorInfoModal/AuthorInfoModal';

const Post = () => {
  const [post, setPost] = useState({});
  const [showCommentModal, setShowCommentModal] = useState(false);
  const [showAuthorInfoModal, setShowAuthorInfoModal] = useState(false);
  const context = useContext(PostsContext);
  const { id } = useParams();
  useEffect(() => {
    async function getPost() {
      const { posts } = context;
      let postIndex;
      if (posts && posts.data) {
        postIndex = posts.data.findIndex(p => p.id === id);
      }

      if (postIndex && postIndex !== -1) {
        return setPost(posts.data[postIndex]);
      }
      const result = await api.get(`${apiRoutes.GET_POSTS}/${id}`);
      return setPost(result.data.data);
    }
    getPost();
  }, [id, context]);

  useEffect(() => {
    const startReading = Date.now();
    return () => {
      const stopReading = Date.now();
      const diffInSec = (stopReading - startReading) / 1000;
      // send time to api
      api
        .put(`${apiRoutes.UPDATE_TIME}/${id}`, {
          time: diffInSec,
        })
        .then(res => {
          console.log('time updated');
        })
        .catch(error => {
          console.log('Soemthing went wrong', error);
        });
    };
  }, [id]);

  const { title, thumbnail, date, content, authorId } = post;

  const handleInfoClick = () => {
    setShowAuthorInfoModal(true);
  };

  const handleCommentBtnClick = () => {
    setShowCommentModal(true);
  };

  return (
    <>
      {showCommentModal && (
        <CommentModal
          show={showCommentModal}
          change={setShowCommentModal}
          id={+id}
        />
      )}
      {showAuthorInfoModal && (
        <AuthorInfoModal
          show={showAuthorInfoModal}
          change={setShowAuthorInfoModal}
          authorId={+authorId}
        />
      )}
      {post.id ? (
        <div className={styles.Post}>
          <h1 className="is-size-2">{title}</h1>
          <img src={thumbnail} alt="thumbnail" />
          <div className={styles.details}>
            <span className="has-text-grey-light">{date}</span>
            <button type="button" onClick={handleInfoClick}>
              i
            </button>
          </div>
          <article>{content}</article>
          <button
            onClick={handleCommentBtnClick}
            type="button"
            className=" button">
            Comment
          </button>
        </div>
      ) : (
        `Loading...`
      )}
    </>
  );
};

export default Post;
