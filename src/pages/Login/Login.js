import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import styles from './Login.module.scss';
import { apiRoutes } from '../../routes';
import api from '../../utils/api';
import AuthContext from '../../context/auth.context';

const USERNAME = 'username';
const PASSWORD = 'password';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const authContext = useContext(AuthContext);
  const history = useHistory();

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const res = await api.post(apiRoutes.AUTH, {
        username,
        password,
      });
      const { data } = res.data;
      if (data && data.token) {
        localStorage.setItem('token', data.token);
        authContext.setIsAuth(true);
        history.push('/');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = e => {
    const changeName = e.target.name;
    if (changeName === USERNAME) {
      return setUsername(e.target.value);
    }
    if (changeName === PASSWORD) {
      return setPassword(e.target.value);
    }
  };
  return (
    <div className={styles.Login}>
      <h1 className="is-size-3 has-text-centered">Simple Web App</h1>
      <div className="box has-text-centered">
        <form onSubmit={handleSubmit}>
          <div className="field">
            <label className="label" htmlFor="username">
              Username
            </label>
            <div className="control">
              <input
                className="input"
                type="text"
                id="username"
                name={USERNAME}
                value={username}
                onChange={handleChange}
                required
                placeholder="Username"
              />
            </div>
          </div>
          <div className="field">
            <label className="label" htmlFor="password">
              Password
            </label>
            <div className="control">
              <input
                className="input"
                type="password"
                id="password"
                value={password}
                name={PASSWORD}
                onChange={handleChange}
                required
                placeholder="Password"
              />
            </div>
          </div>
          <button type="submit" className="button is-primary">
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
