import React, { useState, useEffect, useContext } from 'react';
import { apiRoutes } from '../../routes';
import api from '../../utils/api';
import styles from './MainPage.module.scss';
import List from '../../components/List/List';
import PostsContext from '../../context/posts.context';
import Pagination from '../../components/Pagination/Pagination';

const MainPage = () => {
  const [viewType, setViewType] = useState('list');
  const [dateFilter, setDateFilter] = useState('');
  const [titleFilter, setTitleFilter] = useState('');
  const [pagination, setPagination] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const context = useContext(PostsContext);
  const { posts, setPosts } = context;

  useEffect(() => {
    async function fetchPosts() {
      let route = `${apiRoutes.GET_POSTS}`;

      if (currentPage) {
        route = `${apiRoutes.GET_POSTS}?page=${currentPage}`;
      }

      await api
        .get(route)
        .then(({ data }) => {
          console.log('posts', data);
          setPosts(data.data);
          setPagination(data.pagination);
          setCurrentPage(data.pagination.page);
          setDateFilter('');
          setTitleFilter('');
        })
        .catch(error => {
          console.log(error);
        });
    }
    fetchPosts();
  }, [currentPage, setPosts]);

  const handleSortByName = e => {
    const { value } = e.target;

    const postsData = posts.sort((a, b) => {
      if (value === 'a-z') {
        return a.title.localeCompare(b.title);
      }

      if (value === 'z-a') {
        return b.title.localeCompare(a.title);
      }

      return a;
    });

    setPosts(postsData);
    setTitleFilter(value);
  };

  const handleSortByDate = e => {
    const { value } = e.target;

    const postsData = posts.sort((a, b) => {
      const dateA = new Date(a.date);
      const dateB = new Date(b.date);

      if (value === 'asc') {
        if (dateA < dateB) {
          return -1;
        }
      }

      if (value === 'desc') {
        if (dateA > dateB) {
          return -1;
        }
      }

      if (dateA === dateB) {
        return 0;
      }

      return 1;
    });

    setPosts(postsData);
    setDateFilter(value);
  };

  return (
    <div className={styles.MainPage}>
      <div className={styles.settings}>
        <div className={styles.filters}>
          <div className="field">
            <label className="label">Sort by title</label>
            <div className="select">
              <select value={titleFilter} onChange={handleSortByName}>
                <option value="">Sort by title</option>
                <option value="a-z">A-Z</option>
                <option value="z-a">Z-A</option>
              </select>
            </div>
          </div>
          <div className="field">
            <label className="label">Sort by date</label>
            <div className="select">
              <select value={dateFilter} onChange={handleSortByDate}>
                <option value="">Sort by date</option>
                <option value="desc">Newest to oldest</option>
                <option value="asc">Oldest to newest</option>
              </select>
            </div>
          </div>
        </div>
        <div>
          <div className="field">
            <label className="label">View</label>
            <div className="select">
              <select
                value={viewType}
                onChange={e => setViewType(e.target.value)}>
                <option value="list">List</option>
                <option value="grid">Grid</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <main>
        <List posts={posts} view={viewType} />
      </main>
      {pagination && (
        <div className={styles.Pagination}>
          <Pagination
            totalPages={pagination.totalPages}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      )}
    </div>
  );
};

export default MainPage;
