import React from 'react';
import PropTypes from 'prop-types';
import Navbar from '../components/Navbar/Navbar';
import styles from './MainTemplate.module.scss';

const inlineStyle = {
  display: 'flex',
  flexDirection: 'column',
  height: '100vh',
};

const MainTemplate = ({ children }) => (
  <div style={inlineStyle}>
    <Navbar />
    <div className={styles.MainWrapper}>{children}</div>
  </div>
);

MainTemplate.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainTemplate;
