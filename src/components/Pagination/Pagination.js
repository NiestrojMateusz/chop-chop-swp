/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Pagination.module.scss';

const Pagination = ({ currentPage, totalPages, setCurrentPage }) => {
  const [inputValue, setInputValue] = useState(currentPage);
  const handleChangePage = page => {
    setCurrentPage(page);
    setInputValue(page);
  };

  return (
    <nav
      className={`pagination ${styles.Pagination}`}
      role="navigation"
      aria-label="pagination">
      <a
        className="pagination-previous has-background-white"
        title="This is the first page"
        onClick={() => handleChangePage(currentPage - 1)}
        {...(currentPage === 1 && { disabled: true })}>
        Previous
      </a>
      <a
        className="pagination-next has-background-white"
        onClick={() => handleChangePage(currentPage + 1)}
        {...(currentPage === totalPages && { disabled: true })}>
        Next page
      </a>
      <ul className="pagination-list">
        <li>
          <input
            type="text"
            className={`${styles.paginationInput} has-background-white`}
            value={inputValue}
            onChange={e => setInputValue(e.target.value)}
            onBlur={e => handleChangePage(inputValue)}
          />
        </li>
        <span>/</span>
        <li>
          <a
            className="pagination-link"
            aria-label="Goto last page"
            onClick={() => handleChangePage(totalPages)}>
            {totalPages}
          </a>
        </li>
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
};

export default Pagination;
