import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';
import api from '../../utils/api';
import { apiRoutes } from '../../routes';

import styles from './CommentModal.module.scss';

const CommentModal = ({ show, change, id }) => {
  const [name, setName] = useState('');
  const [comment, setComment] = useState('');
  const [accept, setAccept] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      await api.post(apiRoutes.ADD_COMMENT, {
        name,
        comment,
        id,
      });
      change(false);
    } catch (error) {
      console.log(error);
      change(false);
    }
  };
  return (
    <div className={styles.CommentModal}>
      <Modal isActive={show} change={change}>
        <div className="box">
          <h1 className="is-size-3">Add comment</h1>
          <form onSubmit={null}>
            <div className="field">
              <label className="label">Name</label>
              <div className="control">
                <input
                  value={name}
                  onChange={e => setName(e.target.value)}
                  className="input"
                  type="text"
                  required
                  placeholder="Your Name"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Comment</label>
              <div className="control">
                <textarea
                  value={comment}
                  onChange={e => setComment(e.target.value)}
                  className="textarea"
                  required
                  placeholder="Your comment"
                />
              </div>
            </div>
            <label className="checkbox">
              <input
                value={accept}
                onChange={e => setAccept(e.target.value)}
                required
                type="checkbox"
                style={{ marginRight: '8px' }}
              />
              I Accept
            </label>
            <div className={styles.buttons}>
              <button
                type="submit"
                onClick={handleSubmit}
                className="button is-primary">
                Save
              </button>
              <button
                type="button"
                onClick={() => change(false)}
                className="button">
                Cancel
              </button>
            </div>
          </form>
        </div>
      </Modal>
    </div>
  );
};

CommentModal.propTypes = {
  show: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

export default CommentModal;
