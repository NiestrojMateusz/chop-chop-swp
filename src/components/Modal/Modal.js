import React from 'react';
import PropTypes from 'prop-types';

const Modal = ({ children, isActive, change }) => (
  <div className={`modal ${isActive ? 'is-active is-clipped' : ''}`}>
    <div className="modal-background" />
    <div className="modal-content">{children}</div>
    <button
      type="button"
      onClick={() => change(false)}
      className="modal-close is-large"
      aria-label="close"
    />
  </div>
);

Modal.propTypes = {
  children: PropTypes.element.isRequired,
  isActive: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
};

export default Modal;
