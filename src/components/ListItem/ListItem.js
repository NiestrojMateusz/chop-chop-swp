/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import styles from './ListItem.module.scss';
import fadeTransition from '../../transitions/fadeTransition.module.scss';

const ListItem = ({
  id,
  title,
  thumbnail,
  date,
  excerpt,
  content,
  authorId,
  showAuthorInfo,
  setAuthorId,
}) => {
  const [showExcerpt, setshowExcerpt] = useState(false);

  const handleExcerptClick = () => {
    setshowExcerpt(!showExcerpt);
  };

  const handleInfoClick = () => {
    setAuthorId(authorId);
    showAuthorInfo(true);
  };

  return (
    <div className={`${styles.ListItem} box`}>
      <div className={styles.details}>
        <div className={styles.left}>
          <img
            className={styles.thumbnail}
            src={thumbnail}
            alt="post thumbnail"
          />
          <div className={styles.postMainInfo}>
            <span className="has-text-grey-light">{date}</span>
            <Link
              to={`/post/${id}`}
              className={`is-size-4 ${styles.postTitle}`}>
              {title}
            </Link>
          </div>
        </div>
        <div className={styles.right}>
          <button onClick={handleExcerptClick} type="button">
            e
          </button>
          <button type="button" onClick={handleInfoClick}>
            i
          </button>
        </div>
      </div>
      <CSSTransition
        in={showExcerpt}
        unmountOnExit
        timeout={200}
        classNames={fadeTransition}>
        <div className={styles.excerpt}>{excerpt}</div>
      </CSSTransition>
    </div>
  );
};

ListItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  authorId: PropTypes.number.isRequired,
  showAuthorInfo: PropTypes.func.isRequired,
  setAuthorId: PropTypes.func.isRequired,
};

export default ListItem;
