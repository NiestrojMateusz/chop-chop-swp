import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ListItem from '../ListItem/ListItem';

import styles from './List.module.scss';
import GridListItem from '../GridListItem/GridListItem';
import AuthorInfoModal from '../AuthorInfoModal/AuthorInfoModal';

const List = ({ posts, view }) => {
  const [showAuthorInfoModal, setShowAuthorInfoModal] = useState(false);
  const [currentAuthorInfo, setCurrentAuthorInfo] = useState(null);

  return (
    <>
      {showAuthorInfoModal && (
        <AuthorInfoModal
          show={showAuthorInfoModal}
          change={setShowAuthorInfoModal}
          authorId={currentAuthorInfo}
        />
      )}
      <div className={`${styles.List} ${view === 'grid' ? styles.grid : ''}`}>
        {posts.map(({ id, authorId, ...otherProps }) =>
          view === 'list' ? (
            <ListItem
              key={id}
              id={+id}
              authorId={+authorId}
              {...otherProps}
              showAuthorInfo={setShowAuthorInfoModal}
              setAuthorId={setCurrentAuthorInfo}
            />
          ) : (
            <GridListItem key={id} id={+id} {...otherProps} />
          )
        )}
      </div>
    </>
  );
};

List.propTypes = {
  posts: PropTypes.array,
  view: PropTypes.oneOf(['grid', 'list']),
};
List.defaultProps = {
  posts: [],
  view: 'list',
};

export default List;
