import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';
import styles from './AuthorInfoModal.module.scss';
import api from '../../utils/api';
import { apiRoutes } from '../../routes';

const AuthorInfoModal = ({ show, change, authorId }) => {
  const [author, setAuthor] = useState({});
  useEffect(() => {
    async function fetchData() {
      try {
        const result = await api.get(`${apiRoutes.GET_AUTHOR}/${authorId}`);
        return setAuthor(result.data.data);
      } catch (error) {
        console.log(error);
        change(false);
      }
    }
    fetchData();
  }, [authorId, change]);

  const { avatar, description, id, name } = author;

  const handleClose = () => {
    change(false);
  };
  return (
    <div className={styles.AuthorInfoModal}>
      <Modal isActive={show} change={change}>
        <div className={`box ${styles.wrapper}`}>
          {id ? (
            <>
              <div className={styles.titleSection}>
                <h1 className="is-size-3">{name}</h1>
                <img
                  src={avatar}
                  alt="Thumbnail"
                  className={styles.thumbnail}
                />
              </div>
              <div>{description}</div>
              <button
                type="button"
                className={`button ${styles.closeBtn}`}
                onClick={handleClose}>
                Close
              </button>
            </>
          ) : (
            <span>Loading...</span>
          )}
        </div>
      </Modal>
    </div>
  );
};

AuthorInfoModal.propTypes = {
  show: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  authorId: PropTypes.number.isRequired,
};

export default AuthorInfoModal;
