/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { ReactComponent as Logo } from '../../assets/logo-3.svg';
import styles from './Navbar.module.scss';
import { routes } from '../../routes';
import AuthContext from '../../context/auth.context';

const Navbar = () => {
  const authContext = useContext(AuthContext);
  const history = useHistory();

  const handleLogout = () => {
    localStorage.removeItem('token');
    authContext.setIsAuth(false);
    history.push(routes.LOGIN);
  };
  return (
    <nav
      className="navbar is-primary"
      role="navigation"
      aria-label="main navigation">
      <div className="navbar-brand">
        <Link className={styles.logoContainer} to={routes.MAIN}>
          <Logo className="logo" />
        </Link>
      </div>
      <div className="navbar-menu">
        <div className="navbar-end">
          {!authContext.isAuth ? (
            <Link className="navbar-item" to={routes.LOGIN}>
              Login
            </Link>
          ) : (
            <a className="navbar-item" onClick={handleLogout}>
              Logout
            </a>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
