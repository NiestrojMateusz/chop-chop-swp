/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import styles from './GridListItem.module.scss';

const GridListItem = ({ id, title, thumbnail }) => {
  const history = useHistory();

  const handleCardClick = () => {
    history.push(`/post/${id}`);
  };
  return (
    <div className={`${styles.GridListItem} box`} onClick={handleCardClick}>
      <span className="is-size-6">{title}</span>
      <img src={thumbnail} className={styles.thumbnail} alt="thumbnail" />
    </div>
  );
};

GridListItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
};

export default GridListItem;
